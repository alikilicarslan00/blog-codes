import requests
import base64

def send_image_to_endpoint(image_path, endpoint_url):
    try:
        # Read the image in binary mode
        with open(image_path, 'rb') as file:
            image_data = file.read()

        # Check the original size of the image
        original_size = len(image_data)
        print("Original image size:", original_size)

        # Encode the image as base64 without considering any additional information
        encoded_image = base64.b64encode(image_data).decode('ascii')

        # Send the image directly as binary within the request instead of within JSON
        headers = {'Content-Type': 'application/octet-stream'}  # Set the request header to JSON
        response = requests.post(endpoint_url, data=encoded_image, headers=headers)

        # Check the response
        if response.status_code == 200:
            print("Image sent successfully.")
        else:
            print("An error occurred while sending the image:", response.text)
    except Exception as e:
        print("An error occurred:", str(e))

# Specify the file path of the image
image_path = "./ust-kisim.png"

# Specify the URL where you'll send the image
endpoint_url = "https://vwfja7gjc5.execute-api.eu-central-1.amazonaws.com/deneme-api/api"

# Call the function
send_image_to_endpoint(image_path, endpoint_url)
