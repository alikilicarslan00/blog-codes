import json
import base64
import boto3
import datetime

def lambda_handler(event, context):

    body = event['body']  # Extract 'body' from the content
    
    # Get the current date and time
    now = datetime.datetime.now()
    
    # It's highly likely that the original data of the image comes as a base64 encoded string
    binary_data = base64.b64decode(body)
    original_size = len(binary_data)
    print("Original image size:", original_size)
    
    # Use the Metadata parameter to examine additional metadata added when uploading data to S3
    s3 = boto3.client('s3')
    bucket_name = 'deneme1-blog'
    date_name = f"deneme-klasor/{now}.png"  # Create the name of the image with date and time info
    s3.put_object(Bucket=bucket_name, Key=date_name, Body=binary_data, Metadata={})
    
    return {
        'statusCode': 200,
        'body': json.dumps('File received and saved successfully.')
    }
